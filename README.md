# Revertir y pushear

Esto para cuando te la mandas grosa y tenes q volver a las apuradas a un commit anterior

```bash
git log
```
Lista los commits, seleccionamos al cual tenemos q volver y hacemos

```bash
git revert fb9b52654362116cb083d8d1e04denjkleknm
```

Verificamos los cambios y si esta OK pusheamos
```bash
git push origin main
```
